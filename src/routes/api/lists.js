export const get = async () => {
  return {
    status: 200,
    body: ['pokemon.en', 'ssbu.en'].map((id) => ({
      id,
      uri: `/assets/${id}.json`,
    })),
  };
};
