export const origin = 'https://skribbl.io:4999';

export const params = {
  avatar: [9, 10, 23, -1],
  code: '',
  createPrivate: true,
  join: '',
  language: 'English',
  name: 'Animator',
};

export const uri = import.meta.env.VITE_SERVICE_URI;
